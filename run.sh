#
# Retrotube start-up script
# (c) 2023 eric.yape@gmail.com 
#

if [ $# != 1 ];
then
    echo "ERROR: usage = $0 /path/to/mpd/configuration/file !"
    exit 1
fi

MPD_CONF="$1"

if [ ! -r "$MPD_CONF" ];
then
    echo "ERROR: mpd configuration file ($MPD_CONF) not readable !"
    exit 2
fi

# fetch variables from MPD configuration file
MUSIC_DIRECTORY=`cat "$MPD_CONF" | grep '^music_directory' | grep -oh '".*"'`
STICKER_FILE=`cat "$MPD_CONF" | grep '^sticker_file' | grep -oh '".*"'`
PID_FILE=`cat "$MPD_CONF" | grep '^pid_file' | grep -oh '".*"'`

# trim double quotes
MUSIC_DIRECTORY=${MUSIC_DIRECTORY:1:-1}
STICKER_FILE=${STICKER_FILE:1:-1}
PID_FILE=${PID_FILE:1:-1}

echo "MPD MUSIC_DIRECTORY=$MUSIC_DIRECTORY"
echo "MPD STICKER_FILE=$STICKER_FILE"
echo "MPD PID_FILE=$PID_FILE"

if [ -z "$MUSIC_DIRECTORY" ] || [ -z "$STICKER_FILE" ] || [ -z "$PID_FILE" ];
then
    echo "ERROR: mpd variables music_directory, sticker_file or pid_file not parsable !"
    exit 3
fi

if [ ! -d "$MUSIC_DIRECTORY" ] || [ ! -r "$STICKER_FILE" ];
then
    echo "ERROR: folder 'music_directory' ($MUSIC_DIRECTORY) or file 'sticker_file' ($STICKER_FILE) do not exist !"
    exit 4
fi

echo "# Docker env variables for Retrotube" > docker.env
echo "MUSIC_DIRECTORY=\"$MUSIC_DIRECTORY\"" >> docker.env
echo "STICKER_FILE=\"$STICKER_FILE\"" >> docker.env

DOCKER=`sudo systemctl is-active docker`

if [ "$DOCKER" = "inactive" ];
then
    # start docker
    echo "Starting Docker..."
    sudo systemctl start docker
fi

if [ ! -f "$PID_FILE" ];
then
    # start MPD
    echo "Starting MPD..."
    mpd
fi

echo "Starting Retrotube..."
sudo docker compose --env-file docker.env up