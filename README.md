# Retrotube

**Retrotube** is a web application that enables you to control your music library with a gorgeous user interface! The main view of **Retrotube** displays all your album covers and you just have to click on a cover to start playing your music!

![Retrotube](./screenshots/retrotube-1.jpg)

**Retrotube** is purposely designed to highlight albums covers : you may display the album cover in a full screen mode to enjoy art covers while listening to your music.

![Retrotube](./screenshots/retrotube-2.jpg)

### Hot news: **Retrotube** V5.4 is available since January 4 :heart_eyes:

## Release notes

| Versions | Dates | Short descriptions |
| ---------- | ---------- | ---------- |
| V5.4.2 | January 26 2025 | Add a new web search button; add a confirm pop-up before 'Check album covers' action |
| V5.4.1 | January 19 2025 | Fix a bug to allow (very) big covers to be displayed in UI  |
| V5.4 | January 4 2025 | "**Full Tag Set**" edition: full support of any mpc tags! |
| V5.3.1 | December 8 2024 | Minor UI change: same playlist style in 'Front/Back/Dual cover' display modes  |
| V5.3 | November 16 2024 | "**The Mercyful**" edition with new 'Undo' feature! minor UI issue fixes |
| V5.2 | November 11 2024 | "**4th Anniversary**" edition! New 'Dual cover' display mode; updated 'config' view |
| V5.1.2 | October 6 2024 | New 'Retrotube player' button in 'config' view |
| V5.1.1 | December 31 2023 | Minor UI changes in LCD window; 'sticky' album art cover in left panel; simplified 'sync' controller  |
| V5.1 | December 17 2023 | "**The album cover strikes back**" edition with permanent back cover modes! |
| V5.0.2 | December 9 2023 | New 'Playlist length' display in 'Playlists' display mode; Fix UI bug (scroll position not always saved) |
| V5.0.1 | December 2 2023 | New 'Remove track from user playlist' feature; docker compose start-up script |
| V5.0 | November 11 2023 | "**3rd Anniversary**" edition ! Leaving childhood: now feeded with CodeIgniter 4 and PHP 8 ;-) |
| V4&#x21cc;4 | December 26 2022 | "**Well balanced**" edition with swappable UI panels layout! Additional tracks control buttons |
| V4.3 | December 18 2022 | Refined LCD display with additional track info |
| V4.2 | December 3 2022 | "**MacOS X (Catalina) compatibility**" edition! |
| V4.1 | November 29 2022 | Updated 'Timeframe' mini control; New 'config' view; new 'RT_FAST_COVER_CHECK' configuration parameter |
| V4.0 | October 1 2022 | Major "**Back and Fourth**" edition with back album cover art feature ! Plus WEBP image format support for album art images; New 'Keep' mini control; Ship with Code Igniter 3.1.13 and fix an incorrect url encoding/decoding bug |
| V3.6.3 | September 17 2022 | New 'titles' sort option; Fast forward & backward seek |
| V3.6.2 | February 12 2022 | 'Shortcuts' mini control updated; 'About Retrotube' button |
| V3.6.1 | February 5 2022 | New 'Shortcuts' mini control; new 'Maxi Cover' display mode; UI refinements; 'missing function' bugfix |
| V3.6 | January 23 2022 | "**Cover Curation**" edition for album art covers review; **PHP7 backport**; minor UI refinements; fix a bug with the 'sort' utility |
| V3.5.1 | December 12 2021 | New dual room audio synchronization; new 'Play from this track' button in search mode |
| V3.5 | November 12 2021 | "**1st Anniversary**" edition! - ready for dual room audio system! Super fast albums library display |
| V3.4.1 | November 1 2021 | New web search button; web search buttons available in tracks control panel |
| V3.4 | September 12 2021 | Web search buttons; mini covers (thumbnails) for fast library display |
| V3.3.1 | June 1 2021 | Out-of-sync player watchdog; displayed items count |
| V3.3 | May 16 2021 | "**3-3-3tickers**" edition - ready for full stickers management! New sort options; new playlist mini control panel |
| V3.2 | May 13 2021 | New 'Sticker' search option; Retrotube_model.php code refactoring |
| V3.1 | May 1 2021 | New 'Updates' display mode |
| V3.0 | April 28 2021 | New 'modification times' sort option; first step in mpd stickers handling; special char. in track path bugfix  |
| V2.8.4 | February 23 2021 | New 'Expression' search option; new tracks control panel buttons  |
| V2.8.3 | February 14 2021 | Search results in the tracks panel may now be ordered |
| V2.8.2 | February 13 2021 | New buttons to add/insert search results into queue |
| V2.8.1 | February 7 2021 | Save queue as "instant" playlist |
| V2.8 | January 23 2021 | Panels "visibility" buttons |
| V2.7 | January 16 2021 | New UI features (selection & shortcut); player.php code refactoring  |
| V2.6 | January 12 2021 | New display of the search results in 'show-tracks' select mode |
| V2.5 | January 6 2021 | New display mode 'Folder'; new layout of the main upper bar |
| V2.4 | December 30 2020 | New 'local' http mode; enhanced 'tracks' panel (drag&drop, 'queue' display, ...) |
| V2.3 | December 24 2020 | New display mode 'queue' |
| V2.2 | December 21 2020 | "**Jupiter and Saturn Christmas kiss**" edition - ready for Holidays parties (even planets get deconfined!) |
| V2.1.1 | December 19 2020 | Albums covers cached in session data |
| V2.1 | December 16 2020 | Enhanced 'tracks' display mode; playlist reordering with drag&drop |
| V2.0 | December 8 2020 | New UI; 'tracks' display mode; docker file |
| V1.4 | December 1 2020 | Playlists fine grained management |
| V1.3 | November 26 2020 | New display modes ; status LED |
| V1.2 | November 22 2020 | New selection modes |
| V1.1.1 | November 18 2020 | Track timing in LCD |
| V1.1 | November 16 2020 | Support of mpd playlists |
| V1.0.1 | November 15 2020 | New "search artist" button `>` in playlist |
| V1.0 | November 11 2020 | Initial release |


## Build milestones

| Dates | Milestones |
| ---------- | ---------- |
| 2023/11/11 | **Retrotube** is now built as a docker container with a custom `mpc` binary |
| 2023/03/14 | Archlinux distro does not provide PHP7 binaries any more. Then the only available option to run **Retrotube** is to use the [AUR repositories](https://wiki.archlinux.org/title/Arch_User_Repository) |
| 2022/10/01 | The CodeIgniter 3.1.13 update should fix PHP8 compatibility issues (not yet tested) |
| 2022/01/23 | The Code Igniter 3.1.11 framework used to implement **Retrotube** is no more compatible with the latest releases of PHP 8 ! As a consequence **Retrotube** is now using PHP7 |



## Introduction

There is currently a galaxy of different music players and each of them has a set of features that make it different from the others. As a matter of fact, chosing a music player can be a very tedious task if you don't feel comfortable with the way many music players are organized. It also means that chosing a music player is a very serious task ;-)

The design of **Retrotube** is based on a very simple feature: the albums covers are displayed on the main window and you only have to click on a cover to start playing it. If you like this idea then you may continue reading this page ;-)

However this simple design rationale induces two strong constraints:
1. you must have organized your music library around the "concept" of albums (not playlists)
1. you must have collected a beautiful high resolution album art cover file for each of yours albums ;-)

If your music library is one single folder with hundreds of thousands music files in it, **Retrotube** may not be the more suitable music player for you!

Furthermore, **Retrotube** is of no help if you only listen music via streaming servers: **Retrotube** has no internet features!

_But_, if you have carefully organized your music library by artists/albums and if you have patiently collected high resolution covers for all your albums then **Retrotube** may be your next preferred music player!

## The Music library architecture

As previously explained **Retrotube** music library preferred organization is the following:

![Retrotube music library](./screenshots/music-library.png)

### Retrotube: an ode to visual entertainment during music listening ;-)

**Retrotube** is specifically design to enjoy superb album art covers that most artists provide with their albums during music listening. As a matter of fact, many (many) great photographers or artists have contributed to create gorgeous album art covers that are intrisically bound to the album sound material in people's mind. Browsing the Music Library by album cover art is both delightful and efficient because of this natural binding. In a way **Retrotube** enables to rediscover the pleasure of vinyl record browsing with digital music...

### Full size album art cover

Out of the box, **Retrotube** is able to use the following file naming conventions for album art covers:
* cover.jpg
* cover.jpeg
* cover.png
* cover.webp (since V4.0)
* front.jpg
* front.jpeg
* front.png
* front.webp (since V4.0)

But, if you are using an other file naming convention you may just change the SuffixHelper variables in `app/Libraries/SuffixHelper.php`

Depending on your file system properties the letter case of the cover file names should be taken into account (or not)!

Very big art cover image files (approx. larger that 5000x5000 in size) need a special PHP memory configuration to be handled: since V5.4.1 you may adjust the PHP `memory_limit` variable in the `php-RT.ini` file (the default value should be 128M):
```
[PHP]
memory_limit = 256M
```

### Mini cover files (thumbnails)

Since V3.4 **Retrotube** is also using optional mini cover files (thumbnails) to speed up the default `Library` display mode and yet enjoy high resolution album art covers with the `Front cover` display mode. Mini cover files should be ideally 165x162 sized. Unfortunatelly, there is currently no function in the **Retrotube** application to generate these thumbnails but the Linux environment provides all the necessary tools ;-)

1. Install the [ImageMagick](https://imagemagick.org) tools suite to get the `convert` binary
1. Create an executable script file (`chmod u+x create_minicover.sh`) with these lines:
```
#
echo -n "Creating Retrotube mini cover for $1:"
FOLDER=`dirname "$1"`
convert "$1" -resize 165x162^ "$FOLDER/cover.mini.jpg"
echo done
```
1. Launch the mini covers generation over the whole mpd music directory in the folder where the `create_minicover.sh` script is stored:
```
find /path/to/mpd/music/directory -name cover.jpg -exec ./create_minicover.sh {} \;
```

> Change `cover.jpg` in the `find` command and `cover.mini.jpg` in the script if you are using an alternate file naming convention for cover files.

Out of the box, **Retrotube** is able to use the following file naming conventions for mini covers:
* cover.mini.jpg
* cover.mini.jpeg
* cover.mini.png
* cover.mini.webp (since V4.0)
* front.mini.jpg
* front.mini.jpeg
* front.mini.png
* front.mini.webp (since V4.0)

As for full size covers, if you are using an other file naming convention you may just change the SuffixHelper variables in `app/Libraries/SuffixHelper.php`

**Retrotube** will use the mini cover files if they are available, but it will however use the regular (full size) cover file for the main library display otherwise. So you don't need to refresh the mini covers each time you add an album in your music library.

### Back cover files

Since V4.0 **Retrotube** may display the back cover album art if they are provided ! Back album art cover is an interesting features to quickly access album track list, album credits, album bar codes & identifiers and additional artistic pictures.

Out of the box, **Retrotube** is able to use the following file naming conventions for back covers:
* back.jpg
* back.jpeg
* back.png
* back.webp

As for full size covers, if you are using an other file naming convention you may just change the SuffixHelper variables in `app/Libraries/SuffixHelper.php`

> Covers, mini covers and back covers may use different image formats for the same album:
* main cover &roarr; '/path/to/mpd/music/directory/Section 25/Retrofit/front.jpg'
* mini cover &roarr; '/path/to/mpd/music/directory/Section 25/Retrofit/cover.mini.png'
* back cover &roarr; '/path/to/mpd/music/directory/Section 25/Retrofit/back.webp'

### Covers, mini covers and back covers optional parameters

1. You may optimize the covers lookup by disabling pointless scan if you are using only one file format for covers and mini covers: **Retrotube** will limit all covers scan to the first declared filename extension found in `app/Libraries/SuffixHelper.php`.
```
define('RT_FAST_COVER_CHECK', true);     // enable fast covers check
```

2. Mini covers handling is also an optional feature (enabled by default) that can be disabled to speed-up music library scan in the `app/Config/Constants.php` file:
```
define('RT_CHECK_MINICOVER', false); // disable mini covers feature
```

3. Back covers handling is also an optional feature (enabled by default) that can be disabled to speed-up music library scan in the `app/config/Constants.php` file:
```
define('RT_CHECK_BACKCOVER', false); // disable back covers feature
```

## The software architecture (aka the "Big Picture")

> The install process may be a bit long and confusing. Then you may considered the following diagram as a guide to help you to understand how things glue together. However, the last release of **Retrotube** (V5) is now hidding most of the running software components inside a docker container...

The development of the **Retrotube** software was only targeted to propose a nice user interface. Therefore, **Retrotube** is using other software modules to achieve a rich and exciting listening experience:
1. the HTML/CSS user interface is rendered with a web browser and a http server
1. the audio files are managed with mpc (Music Player Client) and mpd (Music Player Daemon)
1. the internal logic is implemented with the Code Igniter PHP framework

![Software architecture](./screenshots/software-modules.png)

**Retrotube** is basically a graphical `mpd` client built upon the `mpc` command-line based `mpd` client: it's a client of a client! **Retrotube** provides many `mpc` command-line features with a nice user interface.

## Supported platforms

**Retrotube** is still in a development phase and it has been maily tested on Arch Linux with the Apache http server! However if you are using a legacy Linux distro you may encounter unwanted effects (mainly in the HTML/CSS rendering of the user interface)...

> Since V4.2 **Retrotube** is compatible with MacOS X (Catalina) platform :sunglasses: ! However it has not been thoroughly tested on this environment: expect some unattented behaviour.

## Installation

since :label: V5.0, the **Retrotube** installation process is exclusively using the `docker` tools to dramatically reduce the installation steps. Then, you only need to install and configure the `docker` and `mpd` binaries before setting up **Retrotube** !

### 1 Docker

Install [docker](https://docs.docker.com/) for your Linux distro. You may also install the following (optional) docker plugins:

* buildx
* compose

### 2 Music Player Daemon (mpd)

> **Retrotube** is using  `mpd` (a very efficient sound server) and `mpc` (a command line `mpd` client) to send music files to your audio output.

1. Install [mpd](https://www.musicpd.org/) for your  Linux distro and add these 4 lines to your `mpd` configuration file:
```
# increase mpd response buffer
max_output_buffer_size "16384"
# bind to docker interface
bind_to_address "172.17.0.1"
```

  `172.17.0.1` should be the IP address of the docker local network interface. You may check this value with this command:
```
sudo docker network inspect bridge -f '{{range .IPAM.Config}}{{.Gateway}}{{end}}'
```

2. To check if `mpd` is correctly installed on your system, you may use the standard `mpc` binary (NB: **Retrotube** has its own private version in the `docker` container): `mpc listall` command should output the content of your `mpd` music directory.

### 3 **Retrotube** set-up

1. clone the **Retrotube** project:
```
git clone https://gitlab.com/eric.yape/retrotube.git
cd retrotube
```

2. Setup 2 new shell variables `MUSIC_DIRECTORY` and `STICKER_FILE`. These 2 variables must match the `music_directory` and `sticker_file` parameters of the `mpd` configuration file.
```
MUSIC_DIRECTORY=/path/to/mpd/music/directory
STICKER_FILE=/path/to/mpd/sticker/filename
```

  NB: If your `mpd` installation is **not** using the `sticker` file then edit the `app/Config/Constants.php` file bundled in the `app.tar.gz` archive:

  change
```
// define('RT_STICKER_FILE', false); // disable sticker file
define('RT_STICKER_FILE', '/usr/local/Retrotube/StickerDirectory/sticker.db'); // enable sticker file
```

  into
```
define('RT_STICKER_FILE', false); // disable sticker file
// define('RT_STICKER_FILE', '/usr/local/Retrotube/StickerDirectory/sticker.db'); // enable sticker file
```

3. start docker:\
`sudo systemctl start docker`

4. build the docker image:\
`sudo docker build . -t retrotube`

5. launch the docker image suited to your `mpd` installation (5.a or 5.b):

  a. If your `mpd` installation is **not** using the `sticker` file:
```
sudo docker run --add-host retrotube.local:127.0.0.1 \
    --add-host host.docker.internal:host-gateway \
    -p 8080:80 \
    -v $MUSIC_DIRECTORY:/usr/local/Retrotube/MusicDirectory \
    --rm \
    retrotube
```

  b. If your `mpd` installation is using the `sticker` file:
```
sudo docker run --add-host retrotube.local:127.0.0.1 \
    --add-host host.docker.internal:host-gateway \
    -p 8080:80 \
    -v $MUSIC_DIRECTORY:/usr/local/Retrotube/MusicDirectory \
    --mount type=bind,source=$STICKER_FILE,target=/usr/local/Retrotube/StickerDirectory/sticker.db \
    --rm \
    retrotube
```

Since :label: V5.0.1 you may alternatively use the `docker compose` start-up script:
```
./run.sh /path/to/mpd/configuration/file
```

6. connect a web brower to **Retrotube** and enjoy your music ;-) :\
`firefox http://localhost:8080/Retrotube/index.php/Retrotube`

7. at the end of the listening session, stop the docker Retrotube container and the docker service:
```
sudo docker ps -a
CONTAINER ID   IMAGE       COMMAND                  CREATED         STATUS                     PORTS                                   NAMES
7291eab7c70f   retrotube   "apachectl -D FOREGR…"   6 minutes ago   Up 6 minutes               0.0.0.0:8080->80/tcp, :::8080->80/tcp   peaceful_wright
sudo docker stop 7291eab7c70f
sudo systemctl stop docker
```

Since :label: V5.0.1 you may alternatively use the `docker compose` stop command:
```
sudo docker compose --env-file docker.env stop
sudo docker compose --env-file docker.env rm
```

### 3.1 The local http mode ###

> since :label: V5.0, this mode is not maintained any more...

1. Follow `option #1` instructions (1.1 to 1.5), skip the Apache services installation *but* keep the `/srv/http/Retrotube` folder configuration

2. change the first line of `/usr/local/CI3/application/views/Retrotube/player.php` in order to have:
`const LOCAL_HTTP = true;`

3. `cd` to the `/srv/http` folder and start 3 different PHP internal servers:

* PHP internal web server #1
`cd /srv/http ; sudo -u http php -S localhost:8080`
* PHP internal web server #2
`cd /srv/http ; sudo -u http php -S localhost:8081`
* PHP internal web server #3
`cd /srv/http ; sudo -u http php -S localhost:8082`

4. connect a web brower to **Retrotube**:\
`firefox http://localhost:8080/Retrotube/index.php/Retrotube`

### 3.2 The MacOS X (Catalina) installation

#### Why a specific **Retrotube** version for MacOS ?

Since **Retrotube** is basically a PHP web application, there was theoretically no need for a specific MacOS version ! But there was only one single feature of **Retrotube** that failed to run on MacOS: the call to the binary `mpc` command from PHP ! As a matter of fact, the default Apache `$PATH` variable do not include the location of the `mpc` binary installed by `homebrew` (`/usr/local/bin`). None of the classic methods for changing the Apache `$PATH` environment variable work for MacOS:
* changing `$PATH` in `bash` or `zsh` configurations files &roarr; failed !
* adding a file in `/etc/paths.d` &roarr; failed !
* adding a symlink to `/usr/local/bin/mpc` in `/usr/bin` &roarr; failed !

The reason for that is the `System Integrity Protection` of the latest MacOS versions: `SIP` prevents any modification of the default Apache `$PATH` environment variable. You will need to boot the Mac is a special mode to disable `SIP`!

Then the simplest workaround was to add the full path of the `mpc` binary in the **Retrotube** PHP Model class file: the **Retrotube** MacOS compatibility version now includes a new parameter in the `Constants.php` file with the `mpc` path for MacOS (cf below).

#### MacOS set-up

> Because of hardware availability reasons, the **Retrotube** installation for MacOS has been only tested on MacOS 10.15 Catalina...

> The next steps are using the **Retrotube** latest V4 files

1. Configure the MacOS internal http Apache server with the PHP7 module:
```
# uncomment #LoadModule php7_module libexec/apache2/libphp7.so
sudo vi /etc/apache2/httpd.conf
```
1. Install The MacOS `XCode` [Command Line Tools](https://developer.apple.com/download/more/) that match your MacOS version
1. Install the [homebrew](https://brew.sh/) package manager
1. Install `mpd` with `homebrew`
```
brew install mpd
```
1. Configure the `mdp` server following the `homebrew` [instructions](https://formulae.brew.sh/formula/mpd#default)
1. Install `mpc` with `homebrew`
```
brew install mpc
```
1. Install the `osxfuse` [package](https://github.com/osxfuse/osxfuse/releases)
1. Install the `bindfs` binary from [source](https://bindfs.org/)
1. Install the Code Igniter 3 core files
```
sudo unzip /path/to/retrotube/cloned-folder/CodeIgniter-3.1.13.zip -d /usr/local
cd /usr/local
sudo mv bcit-ci-CodeIgniter-bcb17eb CI3
sudo chown -R john:staff /usr/local/CI3
# john is the current user (you) !
```
1. Install the **Retrotube** (V4.2 or newer) core files with the `update.sh` script (change script user parameters according to your local installation):
```
RT_HTML_FOLDER=/Users/john/Sites/Retrotube
RT_CI_FOLDER=/usr/local/CI3/application
```
1. Carefully choose a Code Igniter `sessions` folder that can be read/write by Apache:
```
sudo mkdir /usr/local/CI_sessions
sudo chown _www:_www /usr/local/CI_sessions
```
1. Edit `/usr/local/CI3/application/config/config.php` accordingly:
```
$config['sess_save_path'] = '/usr/local/CI_sessions';
```
1. Edit `/usr/local/CI3/application/config/constants.php`
```
define('RT_MPC_BIN', '/usr/local/bin/mpc');   // MacOS 'ad-hoc' mpc binary`
```
1. Bind the mpd music library with the Apache music folder with `bindfs`:
```
sudo bindfs --map=501/70:@20/@70 /Users/john/Music/ /Users/john/Sites/Retrotube/MusicLibrary
```
where:
* 501 is `john` (you) system user id
* 70 is `_www` (Apache) system user id
* 20 is `john` (you) system group id
* 70 is `_www` (Apache) system group id
* `/Users/john/Music/` is the mpd music library
* `/Users/john/Sites/Retrotube/MusicLibrary` is the Apache music folder

## Running **Retrotube**

You are now ready to start **Retrotube**: congratulations !

1. Start `mpd`
1. (Optional) Activate the `bindfs` folders remapping
1. Start `apache`
1. Launch a web brower:

`firefox http://localhost/Retrotube/index.php/Retrotube`

or, with the Docker installation:\
`firefox http://localhost:8080/Retrotube/index.php/Retrotube`

... and Tada :sunglasses: !

![Retrotube on MacOS Catalina](./screenshots/retrotube_macos_catalina.jpg)

## User manual

### Launch **Retrotube**

1. Start `docker`: `sudo systemctl start docker`
1. Start `mpd`: `mpd`
1. Start the docker container (cf the installation section for `$MUSIC_DIRECTORY` and `$STICKER_FILE` values):
```
sudo docker run --add-host retrotube.local:127.0.0.1 \
    --add-host host.docker.internal:host-gateway \
    -p 8080:80 \
    -v $MUSIC_DIRECTORY:/usr/local/Retrotube/MusicDirectory \
    --mount type=bind,source=$STICKER_FILE,target=/usr/local/Retrotube/StickerDirectory/sticker.db \
    --rm \
    retrotube
```
> The `run.sh` script in the **Retrotube** cloned folder may be used to start the docker container with the MUSIC_DIRECTORY and STICKER_FILE variables extracted from your `mpd` configuration file
4. Open **Retrotube**: `firefox http://localhost:8080/Retrotube/index.php/Retrotube`


After few seconds, all your albums should be displayed in the central main window and you should be able to control the mpd server with the left control panel.

> At the very first start of **Retrotube** a blank page may be displayed instead of the music library (?). Just refresh the browser page and everything should return in its right place (working on it...)

### Overview of the **Retrotube** User Interface

![Overview of the Retrotube User Interface](./screenshots/retrotube-overview.jpg)

### The left Control Panel

The upper part of the left control panel gathers actions on the `mpd` server and the current track:

![The upper part of left Control Panel](./screenshots/control-panel-1.png)

1. The panel `visibility` buttons:\
![The panel visibility buttons](./screenshots/library-panels-4.png)
cf ![The panels visibility buttons](#the-panels-visibility-buttons)

1. The `mpc #1` command gathers an option menu with the available `mpc` commands and a text field for an optional argument. You can select a `mpc` command in the menu, fill an optional text argument and click the `Exec!` button: the selected command is sent to the primary `mpd` server. You would not use this menu very often since most of the common commands are more conveniently arranged below...
1. The `mpc #2` command (or `sync panel`) gathers a text field where you may give the name of a secondary `mpd` server if you want to synchronize another `mpd` server with your **Retrotube** application in order to have a dual room audio system (since V3.5)!
1. A LCD window with:
* the activity LED (green or red)
* the current audio format (frequency & quantization) (since :label: V5.0)
* the sequence number of the currently played track and the time duration of this track
* the mpd modes:
  * `VOL`: the volume level
  * `RPT`: the repeat mode (on/off)
  * `RND`: the random mode (on/off)
  * `SGL`: the single mode (on/off)
  * `CNS`: the consume mode (on/off)
* the percentage of the track elapsed time (since V4.3)
* the total duration time of the track (since V4.3)
1. The control buttons, from top to bottom, left to right:
* Toggle repeat mode
* Toggle random mode
* Toggle single mode
* Toggle consume mode
* Clear the current playlist
* Volume Up (+5%)
* Volume Down (-5%)
* Play track
* Pause track
* Stop track
* Previous track (ctrl click triggers 15 seconds backward seek)
* Next track (ctrl click triggers 15 seconds forward seek)

**The `sync panel`** (since V3.5)

Since V3.5, the **Retrotube** application is able to synchronize the primary `mpd` server with a secondary `mpd` server in order to have a dual room audio system!

> This configuration assumes that you have 2 mpd servers in the same local area network, each of them providing playback to a dedicated audio system. Their respective `music_directory`, `playlist_directory` and (optional) `sticker_file` ressources should be synchronized, either by sharing the same storage or either by an external tool (such as the `rsync` tool)

In order to synchronize the primary `mpd` server with a secondary `mpd` server, fill the text field with the name of the secondary server (an IP address, a network name or a Unix socket) and click on the `Sync` button.
* When the secondary `mpd` server is synchronized, the network name is displayed with a yellow `#ebcf9d` background:

![The sync panel with a connected secondary mpd](./screenshots/sync-panel-ok.png)

* If the secondary `mpd` server can not be reached (when an invalid IP address is given, for example), the mpd name is displayed with a red `#d6b6a0` background:

![The sync panel with an invalid secondary mpd](./screenshots/sync-panel-ko.png)

* In order to stop the synchronization between the primary and the secondary servers, click on the [ &times; ] button:

![The sync panel with a disconnected secondary mpd](./screenshots/sync-panel-disconnected.png)

> How it works ? A `CLICK` on the [ Sync ] button temporarily stops the playback on the primary server, synchronizes the secondary server playlist and re-starts the playback on the 2 servers at the beginning of the current track.

> Some `mpd` modes can not be used when the primary and the secondary servers are connected: the `shuffle` mode for example!

> When the `mpd` tracks are stored on an external disk drive, there could be a noticeable variable delay when `mpd` servers start the playback if the external storage is in `sleep` mode and this delay may disturb the synchronization. A simple workaround is to click on the [ Previous track ] button to re-start the playback from the beginning of the track...

**The activity LED** (since V1.3)

 In the LCD window, a green/red LED gives the status of the **Retrotube** application:
 * ![The Available status LED](./screenshots/library-available-1.png) the **Retrotube** application is available and ready to handle new user commands

 * ![The Busy status LED](./screenshots/library-busy-1.png) the **Retrotube** application is busy with some activity (most of the time related to views refreshing) and can not temporarily handle new user commands

The lower part of the Left Control panel gathers actions on the current playlist and displays the cover of the playing album:

![The upper part of left Control Panel](./screenshots/control-panel-2.png)

1. The "CD tray": the artist and album names of the current playlist are shown.
2. The list of the tracks in the current playlist. For each track is displayed:
* `#`: the sequence number in the playlist
* ![The track control](./screenshots/track_control.png): moving the pointer over this icon pops up the `track control panel`:
	![The track control panel](./screenshots/track_control_panel.png)
	* The [ &times; ] button removes the track from the playlist
	* The [ &times;&mapstoup; ] button removes all tracks above, down to the selected track
	* The [ &times;&mapstodown; ] button removes all tracks below, starting from the selected track
	* The [ &uharl;1&dharr; ] button keeps only the track in the playlist
	* The [ &#8680; ] button queues the selected track as the next one to play
	* The [ &#8681; ] button queues the selected track as the last one to play
	* The [ &roarr;date ] button searches for the track's date in the **Retrotube** library
	* The [ &roarr;genre ] button searches for the track's genre in the **Retrotube** library
	* The [ &roarr;artist ] button searches for the track's artist in the **Retrotube** library
	* The [ &roarr;album ] button searches for the track's album in the **Retrotube** library
	* The [ &roarr;title ] button searches for the track's title in the **Retrotube** library
* `-Title-`: the artist/album/title of the track
* `time`: the duration of the track
3. The album art cover

> Since V2.8: a `CLICK` on the "CD tray" enables both `Queue` display mode and `show-tracks` select mode

To play a specific track, click on its title in the list. Since V2.1, the ordering of the tracks in the playlist may be changed by drag-and-dropping the tracks in the list.

#### Albums Drag & Drop features

since :label: V5.0 you can drag & drop albums or playlists onto the CD tray or onto the playlist.

* Drag & dropping an album on the CD tray replaces the entire playlist with the selected album:
![drag&drop on the CD tray](./screenshots/player_dragdrop_cdtray.png)

* Drag & dropping an album on the playlist insert the selected album at the drop point:
![drag&drop on the playlist](./screenshots/player_dragdrop_playlist.png)

* Drag & dropping an album after the last track of the playlist add the album to the playlist:
![drag&drop after the last track](./screenshots/player_dragdrop_add.png)

* Drag & dropping a user playlist works the same way:
![drag&drop a user playlist on the CD tray](./screenshots/playlist_dragdrop_cdtray.png)

> When drag & drop-ing a user playlist, avoid dragging the playlist text boxes (title or tracks count) !

### The main view

The goal of the main view is to quickly access the music files: it enables the user to easily sort and filter the albums and to display the whole artists directory. Plus you can display the current album cover to add a nice visual artwork to the listening experience :-)

The upper bar of the main view is divided into several mini control panels:

1. **The panel visibility buttons**

cf ![The panels visibility buttons](#the-panels-visibility-buttons)

2. **The `Display` control panel**

![The Display mini control](./screenshots/library-display-1.png)

The `Display` mini control panel modifies the content of the main view:
* `Library`: the albums are displayed (default display mode)
* `Playlists` (since V1.1): the mpd playlists are displayed. Just click on a playlist to start playing it
* `Queue` (since V2.3): only the albums used in the current queue/playlist are displayed
* `Front cover` (since V5.1): the cover of the current album is displayed
* `Back cover` (since V5.1): the back cover of the current album is displayed (if it is available)
* `Dual cover` (since V5.2): the front and the back covers of the current album are displayed side by side (if a back cover is available)
* `Maxi front` (since V5.1):  the cover of the current album is displayed over the upper bar of the main view
* `Maxi back` (since V5.1): the back cover of the current album is displayed (if it is available) over the upper bar of the main view
* `Maxi dual` (since V5.2): the front and the back covers of the current album are displayed side by side (if a back cover is available) over the upper bar of the main view
* `Date` (since V1.3): the list of tracks dates is displayed. Just click on a date label to start a search in the library with this date as a search criteria
* `Genre` (since V1.3): the list of tracks genres is displayed. Just click on a genre label to start a search in the library with this genre as a search criteria
* `Artist`: the list of tracks artists is displayed. Just click on an artist label to start a search in the library with this artist as a search criteria
* `Album` (since V1.3): the directory of albums is displayed. Just click on an album label to start a search in the library with this album as a search criteria
* `Folder` (since V2.5): the folders of the mpd music library are displayed. Just click on a folder to start a search in the library with this folder as a criteria
* `Sticker` (since V3.3): the directory of mpd stickers keys/values is displayed. Just click on a sticker key/value to start a search in the library whith this key/value as a search criteria

> Several display modes have been removed from the `display` control panel since V5.4:  `originaldate:`, `composer:` , `performer:`, ... These features remain available in the improved `search` control panel along with the whole list of known mpc tags. Since V5.4 the `display` control panel only gathers the main `mpc` tags (date, genre, artist and album) as direct display modes.

3. **The `Shortcuts` control panel** (since V3.6.1)

![The Shortcuts mini control](./screenshots/library-shortcuts-1.png)

The `Shortcuts` control panel gathers buttons that activate several `Retrotube` features in a single click.

* `Update mpd` (since V3.6.2): this button launches a `mpc update` command to update the whole mpd music library
* `Display last updates` (since V3.6): this button is a "shortcut" to display the last updated tracks of the `mpd` server in reverse-chronological order (newer tracks are displayed first). Clicking on this button activates:
	* The `Library` display mode
	* The `modification times` sort type
	* The `desc` sort direction
* `Check album covers` (since V3.6): this button is a "shortcut" for album art covers curation ! Since V5.4.2 a pop-up is displayed to confirm the start of this action. The covers are displayed, sorted by their size (width &times; height in pixel) in ascending order to reveal smallest art cover that need a High Definition replacement. Clicking on this button activates:
	* The `Library` display mode
	* The `cover sizes` sort type
	* The `asc` sort direction
* `Undo`&#8617; (since V5.3): this button cancels the last `mpc` command triggered in the **Retrotube** UI. For example, if an album or a track is inadvertently clicked on the User Interface, replacing or editing the current queue, a click on the `Undo` button restores the last queue state and the last track playback timing. To enable this feature, the current queue and playback states are stored by **Retrotube** each time a user action is triggered in the UI. Yet, if the modification is performed by a remote `mpc` client, the stored queue and playback states are not updated...

> These 2 buttons were previously displayed in the `Display` control panel in V3.6

4. **The `Select` control panel** (since V1.2)

![The Select mini control](./screenshots/library-select-1.png)

The `select` mini control panel modifies the way albums are queued on the mpd server when you click on a cover in the `library` view:
* `clear-add-play`: the current playlist is cleared, the selected album/playlist content is queued on the mpd server and playing is started
* `crop-add-play`: the current playlist is cropped (all songs are removed except for the currently playing song), the selected album/playlist content is queued and playing is started. This mode allows you to add a new album in the MDP queue without music interruption.
* `add-play`: the tracks of the selected album/playlist are added at the end of the current playlist and playing is started
* `insert-play`: the tracks of the selected album are inserted after the current playing track
* `show-tracks`: the tracks of the selected album are displayed in the lower part of the main view, in the `tracks` panel (cf below). The `show-tracks` button is highlighted with specific blue color `#a0bed6`.

![The Select mini control](./screenshots/library-select-2.png)

5. **The `Sort by` control panel**

![The Sort by mini control](./screenshots/library-sort-1.png)

The first option menu enables to sort the music library and the tracks with the following sort keys:
* folders (the default sort key)
* cover sizes (since V3.6)
* albums
* artists/albums
* artists/dates/albums
* dates/albums
* dates/artists/albums
* composers/artists/albums (since V3.3)
* genres/artists/albums (since V3.3)
* performers/artists/albums (since V3.3)
* modification times (since V3.0)
* titles (since V3.6.3)

> The `folders` sort key is the "natural" order of the mpd music_directory folder hierarchy and the sort direction option is not usable (yet) with this key.

> The `cover sizes` sort key is using the size of the album art cover (width &times; height in pixel)

> The `modification times` sort key is using the date and time of last file modification in the mpd database (cf `%mtime%` metadata in the `mpc` documentation). Very useful when you want to display your most recently added albums in `mpd`: use this sort option with the 'Descending' sort direction modifier in `Library`display mode!

The second option menu enables 3 different sort directions:
* Ascending
* Descending
* None (random!)

The actions:
* Click the [ Sort ] button to apply the selected sort
* Click the [ &times; ] button to clear the selected criteria and to refresh the main window

6. **The `Search` control panel**

![The Search mini control](./screenshots/library-search-1.png)

6.1. The first **option menu** enables to select a search criteria among the full list of available `mpc` tags plus "Special Features" [SF]:
* [SF#1] !Any: search a value in any `mpc` tags (album, albumartist, ... , work). Cf the complete list after all [SF].
* [SF#2] !Expression (since V2.8.4): enables the use of a boolean snippet like `((artist == "Kraftwerk") AND (title == "Metall auf Metall"))` as a search criteria. Since the syntax is not particularly intuitive, the [mpc](https://www.musicpd.org/doc/mpc/html/) and [mpd protocol](https://www.musicpd.org/doc/html/protocol.html#filters) documentations are worth reading!
* [SF#3] !Filename: search a track *containing* the given string in all `mpd` music library tracks.
* [SF#4] Folder: search a folder with the given string as its *exact* name in all `mpd` music library folders.
* [SF#5] Sticker (since V3.2): cf 6.2 section below.
* Album
* AlbumArtist
* AlbumArtistSort
* AlbumSort
* Artist
* ArtistSort
* Comment
* Composer
* ComposerSort
* Conductor
* Date
* Disc
* Ensemble
* Genre
* Grouping
* Label
* Location
* Mood
* Movement
* MovementNumber
* MUSICBRAINZ_ALBUMARTISTID
* MUSICBRAINZ_ALBUMID
* MUSICBRAINZ_ARTISTID
* MUSICBRAINZ_RELEASEGROUPID
* MUSICBRAINZ_RELEASETRACKID
* MUSICBRAINZ_TRACKID
* MUSICBRAINZ_WORKID
* Name
* OriginalDate
* Performer
* Title
* TitleSort
* Track
* Work

> The above list of `mpc` tags (excluding [SF]) has been extracted from the `mpc tags` command on January 4th 2025 (V5.4): that list may change according to newer `mpc` released versions

> Caution : your `mpd` server may use a different set of tags since the `mpd` and `mpc` binaries may be separately built with different build environments at different times!

6.2. The **text field** enables you to enter the searched value

Since V3.2, the search mini control enables stickers search: the first text field must contains the sticker key and the second text field must contains the sticker value:

![The Search mini control for stickers](./screenshots/library-search-sticker.png)

Stickers may be updated for selected tracks in the `Stickers control panel` (cf below)

since :label: V5.0, the primary search results may be refined with a secondary search: when searching for a pattern in the mpd database (the word 'disco' in the songs title as in the example below), all matching items are displayed in an option menu hidden in the **text field**. The number of available secondary items is displayed next to the **text field** (`x41` in the given example). Just click on the **text field** to open the secondary search menu and select a more refined option in the list. Then, you may refine the search by clicking on `Search` or on `Exact Search` again.

![The secondary search control](./screenshots/library-search-menu.png)

6.3. The **buttons**:
* Click the [ Search ] button to launch the search
* Click the [ Exact Search ] button to automatically build a search expression from the given search criteria
* Click the [ Dump ] button (since V5.4) to display all the recorded tags of the selected type in the `mpd` music library. A click on an item in the retrieved list of recorded tags enables a secondary search in the `mpd` music directory with this tag as a search key
* Click the [ &times; ] button to clear the search criteria and to refresh the main window

> The [ Dump ] button only works with `mpc` tags, "Special Feature" `Folders` and "Special Feature" `Stickers`: it has no effect with "Special Feature" `!Any`, "Special Feature" `!Expression` and "Special Feature" `!Filename` (hence the `!`)

7. **The `Timeframe` control panel**

![The Timeframe mini control](./screenshots/library-timeframe.png)

Since V4.0 this feature enables to display only the last recent modified tracks.

The option menu displays the following options:
* 1 day
* 2 days
* 3 days
* 4 days
* 5 days
* 6 days
* 1 week
* 2 weeks
* 3 weeks
* 4 weeks
* 1 month
* 2 months
* 3 months
* 4 months
* 5 months
* 6 months
* 1 year

This mini control only keeps the tracks/albums that have been inserted/modified in the timeframe defined by the current date and the selected option.

> Albums are displayed in reverse chronological order

### The display modes

1. **The `Library` display mode**

This is the **Retrotube** default display mode.

![The Libray display mode](./screenshots/retrotube-1.jpg)

The main view displays the albums fetched from the mpd music directory after having applied the `search` and `sort by` filters.

Depending on the active `Select:` action, a `CLICK` on an album art cover starts the listening of this album (with `clear-add-play`), send the album to the `mpd` queue (with `crop-add-play`, `add-play` or `insert-play` ) or displays the tracks of this album (with `show-tracks`). A `SHIFT+CLICK` or a `CTRL+CLICK` on an album art cover is automatically triggering a `show-tracks` action, whatever the `Select:` active action is (since V3.6).

> Since V3.4, with the Firefox browser, scrolling in the music library is also possible with the `space` key (to scroll down) and `shift`+`space` keys (to scroll up).

2. **The `Playlists` display mode**

The `Playlists` display mode (since V1.1):
![The Playlists display mode](./screenshots/retrotube-playlists.jpg)

Just click on a playlist icon to start playing it

3. **The `Queue` display mode**

The `Queue` display mode (since V2.3) with the `show-tracks` select mode (the initial state displays the albums and the tracks from the current queue):
![The Queue display mode](./screenshots/retrotube-queue.jpg)

The current playing album is shown with a yellow shadow (since V2.7):\
![The Queue display mode](./screenshots/display-playing.png)

The `Queue` mode only displays albums that are in the current queue/playlist. If the `show-tracks` select mode is enabled, only the queued tracks of a selected/clicked album are displayed in the `tracks` panel:
![The Queue display mode](./screenshots/retrotube-queue-album.jpg)

The selected/clicked album is shown with a blue shadow (since V2.7):\
![The Queue display mode](./screenshots/display-selected.png)

The search results with the `show-tracks` select mode (since V2.6): the `tracks` panel displays all the tracks returned from the search, while the main view displays the albums that contain these tracks. The example illustrates the search of the `disco` word in tracks `title`.
![The search results](./screenshots/retrotube-search.jpg)

4. **The `Front cover`, `Back cover` and `Dual cover` display modes**

The `Front cover` display mode (with both left control panel and main view):
![The Front cover display mode](./screenshots/retrotube-cover.jpg)

Since V5.1 the `Display` mini control indicates that a back cover is available by underlining the `Back cover`, `Dual cover`, `Maxi back` and `Maxi dual` action buttons:

![The Display mini control if a back cover is available](./screenshots/library-display-backcover-available.png)

Since V4.0, a `CTRL+CLICK` on the album art cover temporarily displays the album art **back cover**, if it is available, as the display stays in `Front cover` mode. The `CTRL+CLICK` action is temporary: the default front cover is displayed again when the UI is refreshed if a mpd event occurs (for example when a new track is started). If no back cover is available, the front cover is displayed instead.

The `Back cover` display mode (with both left control panel and main view):
![The Back cover display mode](./screenshots/retrotube-back.jpg)

Since V5.1, the `Back cover` display mode enables a permanent display of the back cover, even if the UI is refreshed. A `CTRL+CLICK` on the back cover temporarily toggles to the front cover, as the display stays in `Back cover` mode.

The `Dual cover` display mode (with both left control panel and main view):
![The Dual cover display mode](./screenshots/retrotube-dual.jpg)


> A nice **Retrotube** feature is that you don't need to provide exact image sizes for the front and cover pictures: **Retrotube** will neatly layout the 2 images side by side even if they have different sizes!

A ``CLICK`` on the album front or back cover or dual covers activates the `Maxi front` or `Maxi back` or `Maxi dual` display modes.

5. **The `Maxi front`, `Maxi back` and `Maxi dual` display modes**

The `Maxi front` display mode (with both left control panel and main view):
![The Maxi front display mode](./screenshots/retrotube-maxcover.jpg)

Since V4.0 a `CTRL+CLICK` on the album art cover temporarily displays the back cover.

Since V5.1, the `Maxi back` display mode enables a permanent display of the back cover, even if the UI is refreshed:
![The Maxi back display mode](./screenshots/retrotube-maxback.jpg)

The `Maxi dual` display mode (since V5.2):
![The Maxi dual display mode](./screenshots/retrotube-maxdual.jpg)

A `CLICK` on the album front or back cover in the `Maxi front` or `Maxi back` or `Maxi dual` display modes activates the `Queue` display mode

Since V3.4, in the `Front cover` or `Back cover` display modes, the `Browse` mini control panel enables to browse external web links with the current track as a search key:

![The Browse mini control panel](./screenshots/library-browse-links-1.png)

* Cover (displays the album art front cover in a new browser tab)
* Apple Music (since V3.6)
* Bandcamp (since V5.4.2)
* Discogs
* FanArt.tv
* Genius.com (since V3.4.1)
* Google
* Google Image
* MusicBrainz
* Qobuz (since V3.6)

> The content displayed by these external web links belongs to their respective owner and is not endorsed by the **Retrotube** application maintainer!

6. **The `Folder` display mode**

This display mode list the folders of the mpd music library

7. **The mpd tags display modes**

This display modes lists the content of the mpd search tags (date, genre, album artist, album, artist, performer, composer)

The `artist:` display mode:
![The Artists display mode](./screenshots/retrotube-artists.jpg)

Just click on an artist name to start a search in the library with it's name

8. **The stickers display mode**

This display mode lists the sticker/value couples of the mpd stickers database

### Playlist management (since V1.4)

In `Playlists` display mode, a fifth mini control is added to the main view upper bar:
![The Playlists contextual mini control](./screenshots/library-playlists-1.png)

This additional mini control enables 3 actions on mpd internal playlists:
* `new`: creates a new (empty) playlist with the given name in the mpd server
* `save`: save the current playlist into a playlist with the given name into the mpd server
* `remove`: PERMANENTLY deletes  the playlist with the given name from the mpd server

To add a track into a mpd playlists, first activate the `Playlists` display mode and then drag and drop a track from the left control panel onto a mpd playlist image:
![drag and drop to playlist](./screenshots/retrotube-playlists-drag-and-drop.jpg)

> When a track is added to a mpd playlist, **Retrotube** needs to briefly activate the mpd playlist and the current playing track is interrupted! When the mpd playlist is updated, **Retrotube** resumes the track to an approximate point in time after the interruption :-/

To remove a track from a mpd playlist, select the mpd playlist, remove the track with the `track control panel` and then save the mpd playlist with the playlists contextual mini control.

### Tracks audio tags visualization (since V2.1)

The `show-tracks` select mode (since 2.1) enable tracks audio tags to be displayed by clicking on an album or a playlist or by enabling `Queue` display mode:
![The Tracks panel](./screenshots/retrotube-tracks.jpg)

The `tracks` panel contains an upper control panel and a table with the audio tags.

#### Tracks control panel ####

The upper bar of the `tracks` panel displays buttons that enable actions on the  items selected in the Main view and the visibility buttons cf ![The panels visibility buttons](#the-panels-visibility-buttons). The buttons are context specific, depending on the display mode and search results.

1. ##### Tracks control panel in `library` and `playlist` display mode #####

![Tracks control panel in library display mode](./screenshots/tracks_panel_action_buttons_library.png)

The buttons acts the same way as the `select` control panel buttons, plus `update` buttons (since V2.4) that send `update` command to the `mpc` client:
* `clear-add-play` album/playlist
* `crop-add-play` album/playlist
* `add-play` album/playlist
* `insert-play` album/playlist
* `update ...`

The `update` buttons may be used when:
* new albums are added to an artist folder
* new tracks are added to an album folder
* the audio tags of tracks are modified

> The `insert-play` select mode has no effect in `Playlist` display mode since the MPC client does not provide the "insert" function for playlists.

2. ##### Tracks control panel in `Queue` display mode #####

![Tracks control panel in queue display mode](./screenshots/tracks_panel_action_buttons_queue.png)

* The `save` button save the current queue as a new playlist with the current date/time as name.

3. ##### Tracks control panel with search results #####

![Tracks control panel with search results](./screenshots/tracks_panel_action_buttons_search.png)

* The `clear-add-play` button clears the current queue and add the search results to the queue.
* The `crop-add-play` button crops the current queue and add the search results to the queue.
* The `add-play` button add the search results to the queue.
* The `insert-play` button insert the search results after the current playing track in the queue.

4. ##### Stickers control panel #####

![Stickers control panel](./screenshots/tracks_stickers_set.png)

Since V3.0 the additional `stickers control panel` may be used to handle `mpd` stickers attributes for the displayed tracks.

* The first text input field is used to enter the sticker `key`
* The second text input field is used to enter the sticker `value`
* Click the `Set` button to set the sticker key/value for all tracks displayed in the `tracks control panel`
* Click the `Delete` button to delete the sticker key for all tracks displayed in the `tracks control panel`
* The potential existing stickers key/value for the displayed tracks. Click on a key/value button to start a search in the library with this sticker key/value as a search criteria

> Stickers may be set and get for individual tracks with the `mpc sticker` command: cf the [mpc documentation](https://www.musicpd.org/doc/mpc/html/#sticker-commands).

> The stickers management is an optional feature of the mpd server. The sticker database should be already configured in the `mpd.conf` configuration file: cf the [mpd documentation](https://www.musicpd.org/doc/html/user.html#the-sticker-database)

5. ##### Browse control panel #####

![Browse control panel](./screenshots/library-browse-links-1.png)

Since V3.4.1 the additional `Browse control panel` enables to display the local album art cover and to browse external web links with the current track as a search key:

* Cover (displays the local album art cover in a new browser tab)
* Apple Music
* Bandcamp (since V5.4.2)
* Discogs
* FanArt.tv
* Genius.com (since V3.4.1)
* Google
* GoogleImage
* MusicBrainz
* Qobuz

> The content displayed by these external web links belongs to their respective owner and is not endorsed by the **Retrotube** application maintainer!

#### Audio tags table ####

![The audio tags table](./screenshots/library-tracks-1.png)

Since V2.1, the `tracks` panel displays the main audio tags for the tracks of the selected album/playlist/queue or search results and action buttons for each track:
* `#`: the track number
* `idx`: the index of the track (discnumber.tracknumber)
* `date`: the `date` tag value (click on a cell of this column to start a search in the library with this date as a criteria)
* `genre`: the `genre` tag value (click on a cell to start a search in library with this genre as a criteria)
* `albumartist`: the `albumartist` tag value (click on a cell of this column to start a search in the library with this album artist as a criteria)
* `album`: the `album` tag value (click on a cell of this column to start a search in the library with this album as a criteria)
* `artist`: the `artist` tag value (click on a cell of this column to start a search in the library with this artist as a criteria)
* `title`: the `title` tag value of the track (click on a cell of this column to start a search in the library with this title as a criteria)
* `time`: the time/duration of the track

Tracks from the `tracks` panel may be drag-and-dropped to the current queue or to the playlists.

The `tracks` panel also displays the current playing song with a yellow outline (since V2.4)

Click on the `show-tracks` button in the main view upper bar, to clear the `tracks` panel and remove the tracks from the previously selected album or playlist.

Since V2.8.3, the tracks fetched from a search may be ordered by the `sort by` control panel.

If you need to edit the tracks audio tags, use an external software like [MusicBrainz Picard](https://picard.musicbrainz.org/)!

##### Track action buttons in `library` and `show-tracks` display mode #####

![The Track action buttons](./screenshots/track_action_buttons_search.png):
* [ &#8627; ] "Play from this track": clear the current playlist, add the album/playlist and start playing from the selected track
* [ &#8679; ] "clear-add-play" the track: clear the current playlist and add the selected track
* [ &#8682; ] "crop-add-play" the track: crop the current playlist and add the selected track
* [ &#8681; ] "add-play" the track: add the selected track to the end of the current playlist
* [ &#8680; ] "insert-play" the track: insert the selected track after the current playing track

> The audio tags of the `tracks` panel that match the search criteria of the `search` control panel are rendered with a blue background `#a0bed6`.

##### Track action buttons in `playlist` display mode #####

![The Track action buttons](./screenshots/track_action_buttons_playlist.png):
* [ &#8627; ] "Play from this track": clear the current playlist, add the album/playlist and start playing from the selected track
* [ &#8679; ] "clear-add-play" the track: clear the current playlist and add the selected track
* [ &#8682; ] "crop-add-play" the track: crop the current playlist and add the selected track
* [ &#8681; ] "add-play" the track: add the selected track to the end of the current playlist
* [ &#8680; ] "insert-play" the track: insert the selected track after the current playing track
* [ &#x2297; ] "Remove track from playlist" (since :label: V5.0.1): remove the track from the **user** playlist (not the current queue)

> When mpd playlists content are displayed in the `tracks` panel, it may happen that a track file in the playlist does not actually exists in the mpd music directory. As a matter of fact, when you update the mpd music directory, some existing tracks may be removed or renamed and since mpd does not check conflicts with the existing playlist that can contain those removed or renamed files, some inconsistency with the playlist definition may occur! In that case, **Retrotube** displays the name of the wrong track file in the `tracks` panel with a red background `#d6b6a0` (as shown in the above screenshot). To correct this, you need to edit the playlist and fix the wrong/missing file error.

##### Track action buttons in `queue` display mode #####

The track action buttons are identical to those of the Left Control Panel (cf this section above)

![The Track action buttons queue](./screenshots/track_action_buttons_queue.png):
* [ &#8627; ] "Play from this track"
* [ &times; ] "Remove this track"
* [ &times;&mapstoup; ] "Remove tracks down to here"
* [ &times;&mapstodown; ] "Remove tracks from here"
* [ &uharl;1&dharr; ] "Remove all other tracks"
* [ &#8680; ] "Queue this track next"
* [ &#8681; ] "Queue this track last"

### The panels "visibility" buttons

Since V2.8, the 3 panels of the **Retrotube** UI (left control panel, main view and tracks panel) have 2 or 3 additionnal buttons in the upper left corner that control some `visibility` attributes:

![The panel visibility buttons 2](./screenshots/library-panels-4.png)

![The panel visibility buttons 1](./screenshots/library-panels-1.png)

![The panel visibility buttons 3](./screenshots/library-panels-3.png)

* [ &#x21d4; ] : hide/show the adjacent panel. For example, clicks on the [ &#x21d4; ] button in the main panel show/hide the left control panel.  Clicks on the [ &#x21d4; ] button in the left control panel show/hide the main view
* [ &#x21d1; ] : increase the size of the `tracks` panel by using 3 different max-sizes (25%-50%-75% of the main view height)
* [ &#x21d3; ] : decrease the size of the `tracks` panel by using 3 different max-sizes (25%-50%-75% of the main view height)
* [ &#x21af; ] : scroll to the current track (in the left control panel or in the `tracks` panel) or scrolls to the current album (in the main view with `Library` display mode)
* [ &#63; ] (not really a "visibility" button ): open this page !
* [ &#x21cc; ] (since :label: V4.4) Swap left and right UI panels
* [ &#9745; ] (since :label: V4.4) Display the **Retrotube** configuration view
* [ &#8635; ] (since :label: V5.0) Reset **Retrotube** User Interface by clearing all session variables

**Retrotube** with the left control panel hidden and `Front cover` display mode:\
![Retrotube with the left control panel hidden](./screenshots/retrotube-hide-leftcontrolpanel.jpg)

> When the left control panel is hidden, an additional `Player` mini-control is displayed in the upper bar of the main view

> The [ &#x21af; ] button is replacing the deprecated V2.7 feature "click on panel labels to scroll to current track/album"

**Retrotube** with the main view hidden (and FF window resized):\
![Retrotube with the main view hidden](./screenshots/retrotube-hide-mainpanel.jpg)

**Retrotube** with the "left" and "right" panels swapped (since V4.4):\
![Retrotube with the main view hidden](./screenshots/retrotube-swappedUI.jpg)

### The `configuration` view (since V4.1)

![The configuration view](./screenshots/config.jpg)

The `configuration` view is a first attempt to display **Retrotube** configuration parameters. It is available at:

`firefox http://localhost:8080/Retrotube/index.php/Retrotube/config`

* The `Retrotube player` action button displays the **Retrotube** main player.

* All the **Retrotube** configuration constants defined in the `/app/Config/Constants.php` file (stored in the `app.tar.gz` archive) are listed.

* The status of the **Retrotube** connectivity to external binaries (`mpd`, `mpc` and `awk`) used by **Retrotube** is also listed.

## One more thing...

Since you read this page up to here, you deserve to see a cooool **Retrotube** feature: the dynamic synchronization with the `mpd` server! As a matter of fact the **Retrotube** UI is automatically refreshed when the state of the `mpd` server is modified. For example, if a `mpd` client modifies the playlist, or jump to the next track, the user interface of **Retrotube** is refreshed to display the new playlist or the new current track. **Retrotube** is also tracking the state of the `mpd` process: when the `mpd` process is killed or started, the UI is also automatically refreshed.

See it in action:\
![Retrotube dynamic sync](./screenshots/retrotube-dyn-sync.mp4)

## Known bugs

* Starting from V2.0, only one **Retrotube** application should be connected to the HTTP server. This is due to the fact that on some OS (Ubuntu), there is a limit on the number of 'idle' commands sent to a mpd server (the 'idle' command is used to synchronize the **Retrotube** UI with the mpd server). Then the **Retrotube** app kills all `idle` pending commands on the HTTP server...
* From time to time, the **Retrotube** UI is partly out of synchronization with the `mpd` server (i.d. the current track in the left control panel is stalled). This is caused by an incorrect `idle` command. Just click on the `>` (play track) control button to re-sync the UI!
* With the exception of the previous ones: Zarro Boogs!

## Credits

* **Retrotube** is using the [CodeIgniter PHP framework](https://www.codeigniter.com/)

* Code snippets:
1. https://www.jonasjohn.de/snippets/php/color-inverse.htm
2. https://stackoverflow.com/questions/3468500/detect-overall-average-color-of-the-picture
3. https://stackoverflow.com/questions/18936915/dynamically-set-property-of-nested-object/36626858

* Icons/images:
1. The **Retrotube** CD faceplate is a derivative work from a nice CD player image: http://letmegooglethat.com/?q=luxman+cd+player
2. Xfce Elementary icons 'user-available.png' & 'user-busy.png'
3. [Wolfgang Amadeus Mozart : Manuscrit_Autographe_Des_Deux_Menuets_Pour_Orchestre, Public domain, via Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Wolfgang_Amadeus_Mozart_-_Manuscrit_Autographe_Des_Deux_Menuets_Pour_Orchestre_-_K.164_-_sheet_2_of_2.jpg)

## Link and Author

https://gitlab.com/eric.yape/retrotube

eric.yape@gmail.com

(work still in progress)
