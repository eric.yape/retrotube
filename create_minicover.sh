#
echo -n "Creating Retrotube mini cover for $1:"
FOLDER=`dirname "$1"`
convert "$1" -resize 165x162^ "$FOLDER/cover.mini.jpg"
echo done
