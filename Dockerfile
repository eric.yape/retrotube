FROM php:8.2-apache

RUN apt-get update
RUN apt-get install --yes git iputils-ping libicu-dev libjpeg-dev libpng-dev libwebp-dev lynx meson net-tools ninja-build vim zip zlib1g-dev

RUN /usr/local/bin/docker-php-ext-install intl
RUN /usr/local/bin/docker-php-ext-install mysqli
RUN /usr/local/bin/docker-php-ext-configure gd --enable-gd --with-jpeg --with-webp
RUN /usr/local/bin/docker-php-ext-install gd

WORKDIR /usr/local/etc/php/conf.d
COPY php-RT.ini .

WORKDIR /var/www/html
COPY index.php .

WORKDIR /etc/apache2/mods-enabled
RUN ln -s ../mods-available/rewrite.load ./rewrite.load

WORKDIR /usr/local
COPY framework-4.tar.gz .
RUN tar -zxf framework-4.tar.gz
RUN rm framework-4.tar.gz
RUN mv framework-4 CI4
RUN chown -R www-data:www-data CI4

WORKDIR /root
COPY composer-setup.php .
RUN php composer-setup.php
RUN mv composer.phar /usr/local/bin/composer
RUN rm composer-setup.php

WORKDIR /usr/local/MPC
RUN git clone https://github.com/ericyape/mpc.git
WORKDIR /usr/local/MPC/mpc
RUN meson . output
RUN ninja -C output
RUN cp /usr/local/MPC/mpc/output/mpc /usr/local/bin

RUN mkdir -p /usr/local/Retrotube/MusicDirectory
RUN mkdir -p /usr/local/Retrotube/StickerDirectory

WORKDIR /etc/apache2/sites-available
COPY retrotube-vhost.conf retrotube.conf
WORKDIR /etc/apache2/sites-enabled
RUN rm 000-default.conf
RUN ln -s ../sites-available/retrotube.conf ./001-retrotube.conf

WORKDIR /usr/local/CI4
COPY env .env

WORKDIR /usr/local/CI4
COPY app.tar.gz .
RUN tar --overwrite -zxf app.tar.gz
RUN rm app.tar.gz

WORKDIR /usr/local/CI4
COPY public.tar.gz .
RUN tar --overwrite -zxf public.tar.gz
RUN rm public.tar.gz

EXPOSE 80
ENTRYPOINT ["apachectl", "-D", "FOREGROUND"]